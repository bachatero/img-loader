# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Напишите пакет для Composer, который будет загружать картинки с удаленного хоста и сохранять их на Файловой системе. Пакет должен делать все возможные проверки и бросать exceptions в случае исключительных ситуаций. Возможные форматы картинок - jpg, png, gif.

Код комментировать, стиль — psr-2
Автолоадер — psr-4
Комменты — phpdoc (опционально)
Юнит-тесты — phpunit  (опционально)
Выложить на Github.

Норма времени на выполнение теста - 3 часа.
Выложив задание, напишите, пожалуйста, уведомление нам на e-mail recruiter@hexa.com.ua
с темой письма “Middle PHP developer test is taken”. В письме сообщите сколько времени вам потребовалось на выполнение теста.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact